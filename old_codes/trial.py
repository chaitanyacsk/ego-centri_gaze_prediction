import keras
import tensorflow as tf
from keras.layers import Dense, Conv2D, Conv3D, BatchNormalization, Activation
from keras.layers import AveragePooling2D, Input, Flatten,MaxPooling3D,MaxPooling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras import backend as K
from keras.models import Model
from keras.datasets import cifar10

from keras.utils import plot_model
from keras.optimizers import SGD
import numpy as np
import os
from keras.losses import BinaryCrossentropy

from keras.models import Model
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
from keras.models import load_model
from gaze_model import *
from Inputs import *
import os

def pred_coords(predict,test_frames,test_vid=None):
    
    
    predout = predict.squeeze()
    j=0
    predframe = []
    coords_list = get_coords(test_vid)
    for arrframes in predout:
        for frame in arrframes:

            nx = np.argmax((np.max(frame,axis=1)))
            ny = np.argmax((np.max(frame,axis=0)))
    
            x = 320*ny + 160
            y = 180*nx + 90
            try:
                a,b = coords_list[j]
                #print(len(test_frames[i]),len(get_coords(path)) )
                frametemp = cv2.rectangle(test_frames[j],(x-160,y-90),(x+160,y+90),(0, 255, 0), 2)
                predframe.append(cv2.rectangle(frametemp,(a-98,b-98),(a+98,b+98),(255, 0, 0), 2))
                j+=1
            except IndexError:
                pass
            
    return predframe
path = 'test'

model_path = 'model_co.h5'
coarse_mod = coarse_model()
coarse_mod.load_weights(model_path)


def get_coords(video):
    inparr = []
    gridarr=[]
    inpgrid = []
    path_file = path+'/test_labels/'+video[:-4]+'.txt'
    with open(path_file, "r") as f:
       
        
        content = f.readlines()
        for i in range(len(content)):
            
      #grid = np.zeros((4,4))
            x,y = content[i].strip().split(',')[0:2]
            x,y = int(x),int(y)
            inparr.append([x,y])
    return inparr

for video in os.listdir(path+'/'+'test_videos'):
    opframes=[]
    frames = []
    
    test_array,test_frames = get_frames(vidpath=path+'/test_videos/'+video,train=False)
    pred = coarse_mod.predict(test_array)
    print('pred=', len(pred),type(pred),pred.shape,pred.squeeze().shape)
    print('test_array =' ,test_array.shape)
    print('test_frames=',type(test_frames),len(test_frames),type(test_frames[0]),test_frames[0].shape)
    predframe = pred_coords(pred,test_frames,test_vid = video)
    print(type(predframe))
    print(len(predframe))
    a,b = get_coords(video)[0]
    print(a,b)
    print(predframe[0].shape)