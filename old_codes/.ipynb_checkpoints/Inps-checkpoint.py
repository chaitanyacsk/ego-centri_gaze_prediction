import os
import keras
import tensorflow as tf
from keras.layers import Dense, Conv2D, Conv3D, BatchNormalization, Activation
from keras.layers import AveragePooling2D, Input, Flatten,MaxPooling3D,MaxPooling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras import backend as K
from keras.models import Model
from keras.datasets import cifar10

from keras.utils import plot_model
from keras.optimizers import SGD
import numpy as np
import os
from keras.losses import BinaryCrossentropy

from keras.models import Model
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
import matplotlib.pyplot as plt


def get_frames(vidpath,overlapping = False):
    inp = []
    vid_frames = []
    #print(os.listdir(vidpath))
    
    for vid in os.listdir(vidpath):
        #print(vid,'read')
        cap = cv2.VideoCapture(vidpath+'/'+vid)
        opframes=[]
        frames = []
        i=0
       
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            opframes.append(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        #frame = cv2.resize(frame1,(64,64))
            frames.append(cv2.cvtColor(cv2.resize(frame,(64,64)), cv2.COLOR_BGR2RGB))
            
    
        if overlapping:
            for i in range(len(frames)-16):
                inp.append(np.array(frames[i:i+16]))


        #Omitting last frame because gaze coordinates of the last frame are not available 
        else:
            frx = list(range(0,len(frames)-1,16))
       
            for i in range(len(frx)):
                try:
                    inp.append( np.array( frames[frx[i]:frx[i+1]] )) 
                except IndexError:
                    pass
        
        
        vid_frames.append(opframes)
    cap.release()
    cv2.destroyAllWindows()
    #print(len(inp),inp[0].shape,len(frames),frames[0].shape)
    return np.array(inp),vid_frames






def get_grids(path,overlapping = False):
    inparr = []
    inpgrid = []
    gridlist = os.listdir(path)
    vidlist = os.listdir('train/videos')
    newgridlist=[]
    for vid in vidlist:
        gridarr=[]
        
        file = vid[:-4] + '.txt'
        #print(file,'read')
        with open(path+'/'+file, "r") as f:
        
        
            content = f.readlines()
            for i in range(len(content)):
            
                grid = np.zeros((4,4))
                x,y = content[i].strip().split(',')[0:2]
                inparr.append(np.array([int(x),int(y)]))
                grid[int(int(y)//180),int(int(x)//320)]=1
                gridarr.append(grid)
    
        if overlapping:
            for i in range(len(gridarr)-16+1):
                inpgrid.append(np.array(gridarr[i:i+16]))
           
    
        else:
            gdx = list(range(0,len(gridarr),16))
       
            for i in range(len(gridarr)):
                try:
                    inpgrid.append( np.array( gridarr[gdx[i]:gdx[i+1]] )) 
                except IndexError:
                    pass
        
        
            
    
    return np.expand_dims(np.array(inpgrid),axis=-1)


if __name__=='__main__':
    inp_array,frames_array = get_frames('train/videos')
    inp_grid = get_grids('train/labels')
    #print((inp_array).shape)
    #print(type(inp_array))
    print(len(frames_array),frames_array[0][0].shape,len(frames_array[1]))
    #print(len(inp_grid))
    #print((inp_grid).shape)
    #print(frames_array[0].shape,frames_array[1].shape)
    
    print(inp_array.shape)
   