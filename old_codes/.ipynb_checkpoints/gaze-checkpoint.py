import keras
import tensorflow as tf
from keras.layers import Dense, Conv2D, Conv3D, BatchNormalization, Activation
from keras.layers import AveragePooling2D, Input, Flatten,MaxPooling3D,MaxPooling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras import backend as K
from keras.models import Model
from keras.datasets import cifar10
from keras.losses import CategoricalCrossentropy
from keras.utils import plot_model
from keras.optimizers import SGD
import numpy as np
import os
#from keras.losses import BinaryCrossentropy

from keras.models import Model
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
import keras.backend as K

from tensorflow.python.client import device_lib



sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(
        device_count={ 'GPU':10},
        inter_op_parallelism_threads=0,
        allow_soft_placement=True,
        gpu_options= {'allow_growth': True, 'visible_device_list': "0,1,2,3,4,5,6,7,8,9"},
        intra_op_parallelism_threads=0,
    ))

    

print('done')





#######################Architecture####################################
currentFrames = Input(shape=(16,64,64,3))
regularizer = 1e-6

tempConvSize = 3
x = Conv3D(64, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer), 
           padding='same', name='current_conv1')(currentFrames)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool1')(x)
# 2nd layer group
x = Conv3D(128, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv2')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool2')(x)
# 3rd layer group
x = Conv3D(256, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv3a')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(256, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
          padding='same', name='current_conv3b')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool3')(x)
# 4th layer group
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv4a')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv4b')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool4')(x)
# 5th layer group
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv5a')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv5b')(x)
x = BatchNormalization()(x)
exFeat = Activation('relu')(x)

    




x = Conv3D(512, (3, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv6a')(exFeat)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(512, (1, 1, 1), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv6b')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(1, (1, 1, 1), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv6c')(x)
x = BatchNormalization()(x)
cap = Activation('linear')(x)




################################inps######################################

def create_inp(vidpath,overlapping = False):
    inp = []
    print(os.listdir(vidpath))
    for vid in os.listdir(vidpath):
        cap = cv2.VideoCapture('videos/'+vid)
        frames = []
        i=0
       
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frame = cv2.resize(frame,(64,64))
            frames.append(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
    
        if overlapping:
            for i in range(len(frames)-16):
                inp.append(np.array(frames[i:i+16]))
            
        else:
            frx = list(range(0,len(frames)-1,16))
       
            for i in range(len(frx)):
                try:
                    inp.append( np.array( frames[frx[i]:frx[i+1]] )) 
                except IndexError:
                    pass
            
        
        cap.release()
        cv2.destroyAllWindows()

    return inp

# In[118]:


def get_grids(path,overlapping = False):
    inparr = []
    gridarr=[]
    inpgrid = []
    gridlist = os.listdir(path)
    vidlist = os.listdir('videos')
    newgridlist=[]
    for i in vidlist:
        v = gridlist.index(i[:-4] + '.txt')
        newgridlist.append(gridlist[v])
    for j,file in enumerate(newgridlist):
        print(file)
        gridarr=[]
        #print(os.listdir('videos')[j][:-4],os.listdir('labels')[j][:-4],j)
        
        assert os.listdir('videos')[j][:-4]==newgridlist[j][:-4]
        with open('labels/'+file, "r") as f:
       
        
            content = f.readlines()
            for i in range(len(content)):
            
                grid = np.zeros((4,4))
                x,y = content[i].strip().split(',')[0:2]
                inparr.append(np.array([int(x),int(y)]))
                grid[int(int(y)//180),int(int(x)//320)]=1
                gridarr.append(grid)
    
        if overlapping:
            for i in range(len(gridarr)-16+1):
                inpgrid.append(np.array(gridarr[i:i+16]))
           
    
        else:
            gdx = list(range(0,len(gridarr),16))
       
            for i in range(len(gridarr)):
                try:
                    inpgrid.append( np.array( gridarr[gdx[i]:gdx[i+1]] )) 
                except IndexError:
                    pass
        
        
            
    
    return inpgrid
        
        


###################Model###############################################



def custom_loss(y_true,y_pred):
    ce = CategoricalCrossentropy(from_logits=False)
    #bce = BinaryCrossentropy()
    #y_pred = tf.squeeze(y_pred)
    return ce(y_true,y_pred)
        



#print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

train_X = create_inp('videos',overlapping = False)
train_X = np.array(train_X)/255
train_Y = get_grids('labels',overlapping = False)

train_Y = np.expand_dims(train_Y,axis=4)
def shuffle_in_unison(a, b):
    assert len(a) == len(b)
    shuffled_a = np.empty(a.shape, dtype=a.dtype)
    shuffled_b = np.empty(b.shape, dtype=b.dtype)
    permutation = np.random.permutation(len(a))
    for old_index, new_index in enumerate(permutation):
        shuffled_a[new_index] = a[old_index]
        shuffled_b[new_index] = b[old_index]
    return shuffled_a, shuffled_b

train_X,train_Y = shuffle_in_unison(np.array(train_X),np.array(train_Y))

model = Model(inputs=currentFrames, outputs=cap)
#print(model.summary())
#model = Model(inputs=currentFrames, outputs=cap)

opt = SGD(lr=0.0005, momentum=0.9, decay=1e-2)

model.compile(loss =custom_loss,optimizer = opt,metrics = ['accuracy'])

opt = SGD(lr=0.0005, momentum=0.9, decay=1e-2)
print(train_X.shape,train_Y.shape)
model.fit(train_X,train_Y,
          batch_size=16,epochs=30)



# plot graph
#plot_model(model)

model.save_weights("model_e50.h5")

#model.predict(inp)
