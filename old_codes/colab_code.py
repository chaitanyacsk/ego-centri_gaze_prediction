import keras
import tensorflow as tf
from keras.layers import Dense, Conv2D, Conv3D, BatchNormalization, Activation
from keras.layers import AveragePooling2D, Input, Flatten,MaxPooling3D,MaxPooling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras import backend as K
from keras.models import Model
from keras.datasets import cifar10

from keras.utils import plot_model
from keras.optimizers import SGD
import numpy as np
import os
from keras.losses import BinaryCrossentropy
from keras.losses import CategoricalCrossentropy
from keras.models import Model
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os


from keras.utils import plot_model



import keras.backend as K


#######################Architecture####################################
currentFrames = Input(shape=(16,64,64,3))
regularizer = 1e-6

tempConvSize = 3
x = Conv3D(64, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer), 
           padding='same', name='current_conv1')(currentFrames)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool1')(x)
# 2nd layer group
x = Conv3D(128, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv2')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool2')(x)
# 3rd layer group
x = Conv3D(256, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv3a')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(256, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
          padding='same', name='current_conv3b')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool3')(x)
# 4th layer group
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv4a')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv4b')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),
                 padding='valid', name='current_pool4')(x)
# 5th layer group
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv5a')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(512, (tempConvSize, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv5b')(x)
x = BatchNormalization()(x)
exFeat = Activation('relu')(x)

    




x = Conv3D(512, (3, 3, 3), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv6a')(exFeat)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(512, (1, 1, 1), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv6b')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = Conv3D(1, (1, 1, 1), kernel_regularizer=l2(regularizer),
           padding='same', name='current_conv6c')(x)
x = BatchNormalization()(x)
cap = (Activation('linear')(x))





################################inps######################################



# In[118]:



            
        
            
        


###################Model###############################################

#print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

train_X = np.load('frames1.npy')


train_Y = np.load('labels1.npy')


train_Y = np.expand_dims(train_Y,axis=4)
def shuffle_in_unison(a, b):
    assert len(a) == len(b)
    shuffled_a = np.empty(a.shape, dtype=a.dtype)
    shuffled_b = np.empty(b.shape, dtype=b.dtype)
    permutation = np.random.permutation(len(a))
    for old_index, new_index in enumerate(permutation):
        shuffled_a[new_index] = a[old_index]
        shuffled_b[new_index] = b[old_index]
    return shuffled_a, shuffled_b

train_X,train_Y = shuffle_in_unison(np.array(train_X),np.array(train_Y))

model = Model(inputs=currentFrames, outputs=cap)

#model = Model(inputs=currentFrames, outputs=cap)

opt = SGD(lr=0.0005, momentum=0.9, decay=1e-2)

model.compile(loss = 'binary_crossentropy',optimizer = opt,metrics = ['accuracy'])

opt = SGD(lr=0.0005, momentum=0.9, decay=1e-2)

train_X == train_X/255
model.fit(np.array(train_X),train_Y,
          batch_size=16,epochs=20)


#print(model.summary())
# plot graph
#plot_model(model)
     
model.save_weights("model_drive.h5")

#model.predict(inp)
#inp1 = train_X[45]
#ypred = model.predict(inp1[None])
#print(train_Y[45][15].reshape((4,4)))
#print(np.max(ypred))
#print(y_pred[0])
#print(np.min(y_pred))
